using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadBallController : MonoBehaviour
{
    GameObject playrePrefab;
    // Start is called before the first frame update
    void Start()
    {
        this.playrePrefab = GameObject.Find("piayrePrefab");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.1f, 0, 0);//ボールを移動させる
        if (transform.position.x < -20)//左に２０以上行ったら
        {
            Destroy(gameObject);//オブジェクトは消える
        }
        Vector2 p1 = transform.position;//ボールの中心座標
        Vector2 p2 = this.playrePrefab.transform.position;//プレイヤーの中心座標
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;//ボール半径
        float r2 = 1.0f;//プレイヤー半径

        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();
            Destroy(gameObject);
        }
    }
}
