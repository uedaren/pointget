using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    GameObject piayrePrefab;
    // Start is called before the first frame update
    void Start()
    {
        this.piayrePrefab = GameObject.Find("piayrePrefab");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.02f, 0, 0);
        if(transform.position.x<-20)
        {
            Destroy(gameObject);
        }
        Vector2 p1 = transform.position;
        Vector2 p2 = this.piayrePrefab.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.0f;

        if(d<r1+r2)
        {
           
            Destroy(gameObject);
        }
    }
}
