using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject GoodBallPrefab;
    float span = 0.5f;
    float delta = 0;
    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta>this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(GoodBallPrefab) as GameObject;
            int px = Random.Range(-5, 6);
            go.transform.position = new Vector3(13, px, 3);
        }
    }
}
